# Two Dimensional Parity Check
A program used to do two dimensional parity checks and 1-bit EDC (Error Detection and Correction)

## How it works
Either even or odd parity can be used. Each unit is set so that all bytes have either an odd number or an even number of set bits. We can do simple error detection through checking whether or not the received data corresponds with the parity scheme, that is, whether or not there is an even number of bits depending on which scheme was used. Two dimensional parity checking computes the sum for each row and column, which gives it the opportunity to be able to correct the error. This can be done by finding the row and column that do not match the parity scheme, and then flipping the corresponding bit.
