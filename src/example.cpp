/**
 * Copyright 2018 cpke
 */

#include <iostream>
#include "include/paritycheck.h"
#include <vector>
#include <limits>

int main() {
    std::vector< std::vector<int> > vi = {
            { 1, 0, 1, 0, 1, 1 },
            { 1, 1, 1, 1, 0, 0 },
            { 0, 1, 1, 1, 0, 1 },
            { 0, 0, 1, 0, 1, 0 }
    };

    ParityCheck parity_check(Scheme::Even, vi);
    const auto [row_error, column_error] = parity_check.find_parity_error();
    const bool parity_is_valid = (row_error == parity_ok() && column_error == parity_ok());
    if (parity_is_valid) {
        std::cout << "No parity errors found!\n";
    }
    else {
        std::cout << "Parity error at row " << result.first << ", column " << result.second << '\n';
        parityCheck.fix_parity_error(result.first, result.second);
    }

    return 0;
}