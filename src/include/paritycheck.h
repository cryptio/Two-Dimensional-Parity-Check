/**
 * Copyright 2018 cpke
 */

#ifndef INC_2D_PARITY_CHECK_PARITYCHECK_H
#define INC_2D_PARITY_CHECK_PARITYCHECK_H

#include <vector>
#include <map>
#include <string>
#include <utility>
#include <limits>

enum class Scheme {
    Even,
    Odd
};

class ParityCheck {
public:
    ParityCheck(const Scheme &s, const std::vector <std::vector<int>> &bits);

    std::pair<unsigned, unsigned> find_parity_error();

    bool matches_parity(const unsigned &number) const;

    void fix_parity_error(const unsigned &row, const unsigned &column);

    const std::vector <std::vector<int>> &get_bits() const;

    const Scheme &get_scheme() const;

private:
    void set_bit(const unsigned &row, const unsigned &column, const unsigned &value);

    std::vector <std::vector<int>> bits_;
    Scheme scheme_;
};

unsigned parity_ok();


#endif //INC_2D_PARITY_CHECK_PARITYCHECK_H
