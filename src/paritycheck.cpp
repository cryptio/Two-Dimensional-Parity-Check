/**
 * Copyright 2018 cpke
 */

#include "include/paritycheck.h"
#include <numeric>
#include <iostream>

ParityCheck::ParityCheck(const Scheme &s, const std::vector<std::vector<int> > &bits)
        : scheme_(s), bits_(bits) {

}

std::pair<unsigned, unsigned> ParityCheck::find_parity_error() {
    const auto bits = get_bits();

    const unsigned length = bits.size(), width = bits[0].size();
    unsigned row_sum = 0, column_sum = 0;
    unsigned row_error = parity_ok(), column_error = parity_ok();

    /* Calculate the sums for each row */
    for (auto i = 0; i < length; ++i) {
        for (auto j = 0; j < width; ++j) {
            row_sum += bits[i][j];
        }
        if (!matches_parity(row_sum)) {
            row_error = i;
            break;
        }
    }

    /* Calculate the sums for each column */
    for (auto i = 0; i < width; ++i) {
        for (auto j = 0; j < length; ++j) {
            column_sum += bits[j][i];
        }
        if (!matches_parity(column_sum)) {
            column_error = i;
            break;
        }
    }

    return {row_error, column_error};
}

bool ParityCheck::matches_parity(const unsigned &number) const {
    if (get_scheme() == Scheme::Even)
        return number % 2 == 0;
    else
        return number % 2 != 0;
}

void ParityCheck::fix_parity_error(const unsigned &row, const unsigned &column) {
    switch(get_bits()[row][column]) {
        case 0:
            set_bit(row, column, 1);
            break;
        case 1:
            set_bit(row, column, 0);
            break;
        default:
            throw std::logic_error("Encountered invalid parity bit\n");
    }
}

const std::vector<std::vector<int> > &ParityCheck::get_bits() const {
    return bits_;
}

const Scheme &ParityCheck::get_scheme() const {
    return scheme_;
}

void ParityCheck::set_bit(const unsigned &row, const unsigned &column, const unsigned &value) {
    if ((value > 1) || (value < 0))
        throw std::invalid_argument("Invalid value for bit");

    const unsigned length = bits_.size(), width = bits_[0].size();

    if ((row > length) || (column > width))
        throw std::invalid_argument("Index does not exist");


    bits_[row][column] = value;
}

unsigned parity_ok() {
    return std::numeric_limits<unsigned>::max();
}
